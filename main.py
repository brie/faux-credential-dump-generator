import argparse
import datetime
import os
import passgen
from faker import Faker
fake = Faker()
import requests


def upload_ghostbin(abs_cred_file):
    # Read contents of abs_cred_file for the payload below
    file = open(abs_cred_file)
    creds_to_upload = file.read()  # .replace("\n", " ")
    file.close()
    url = "https://ghostbin.site/paste/new"
    the_user_agent = fake.user_agent()
    payload = "text=" + creds_to_upload + "&title=Passwords&password="
    headers = {
        'authority': "ghostbin.site",
        'cache-control': "max-age=0",
        'origin': "https://ghostbin.site",
        'upgrade-insecure-requests': "1",
        'dnt': "1",
        'content-type': "application/x-www-form-urlencoded",
        'user-agent': the_user_agent,
        'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        'sec-fetch-site': "same-origin",
        'sec-fetch-mode': "navigate",
        'sec-fetch-user': "?1",
        'sec-fetch-dest': "document",
        'referer': "https://ghostbin.site/",
        'accept-language': "en-US,en;q=0.9",
        'sec-gpc': "1"
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    link_to_paste = response.url
    return link_to_paste


def make_email_address(righthandside):
    # Let's say an email address is a first name, a last name and a domain name. 
    first_name = fake.first_name().lower()
    last_name = fake.last_name().lower()
    email_addr = first_name + "." + last_name + "@" + righthandside
    return email_addr

def main(accounts, cred_file, righthandside, delimiter, feature):
    if feature:
        print("Cool. I will upload when done.")
    if not feature:
        print("FINE. I won't upload.")
    cred_dump = list()
    print("The right hand side of the generated email addresses will be " + righthandside)
    for i in range(accounts):
        email = make_email_address(righthandside)
        password = passgen.passgen(punctuation=True)
        cred = email+delimiter+password
        cred_dump.append(cred)
    with open(cred_file, mode='w') as f:
        f.write("\n".join(cred_dump))
    abs_cred_file = os.path.abspath(cred_file)
    now = datetime.datetime.now()
    print(f"Jettison Credentials:   {now}")
    print("*" * 72)
    print(f"Accounts Jettisoned:    {accounts}")
    print(f"Location:               {abs_cred_file}")
    print("*" * 72)
    if feature:
        print("Uploading to ghostbin.site...")
        link_to_paste = upload_ghostbin(abs_cred_file)
        print("Done uploading: see " + link_to_paste + ".")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""This is a small Python program that generates a credential dump 
    similar to what one might find on Pastebin. This uses the Faker to generate left hand sides of email addresses
    and accept user input for the right-hand side of the email address. Passwords are generated via the passgen 
    module.""")
    parser.add_argument("accounts", type=int, help="The number of accounts to include.")
    parser.add_argument("credfile", help="The filename to write out to.")
    parser.add_argument("righthandside", help="The right hand side (domain name) to use in the generated credentials.")
    parser.add_argument("delimiter", default=":", nargs="?", help="The delimiter between the email address and the password.")
    parser.add_argument('--feature', dest='feature', action='store_true')
    parser.add_argument('--no-feature', dest='feature', action='store_false')
    parser.set_defaults(feature=True)
    args = parser.parse_args()
    main(args.accounts, args.credfile, args.righthandside, args.delimiter, args.feature)
