# faux-credential-dump-generator

A small Python program that generates a faux credential dump. This might be useful for testing Yara rules or other search and monitor tools.

## Usage
Install the prerequisites `pip install -r requirements.txt`
Specify the number of accounts to include and the path to a file (present directory assumed unless a full path is provided)

## Example
Run `python main.py 40 howdy dunder-mifflin.com` to get forty email address password pairs in a file in `/tmp` called `credsbackup_20190803`.


[![asciicast](https://asciinema.org/a/253253.svg)](https://asciinema.org/a/253253)
